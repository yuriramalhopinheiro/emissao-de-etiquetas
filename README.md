Projeto para emissão de etiquetas para a loja Cacau Show de Araçuaí-MG.

Desenvolvido por Yuri Ramalho Pinheiro

Objetivo: 
    Criar etiquetas para identificar o produto pelo nome e preço, automaticamente, 
    quando o usuário informar o nome e preço do produto. 
    Elas serão impressas para anexar aos produtos da loja.
    
Tecnologias

    JSF e Primefaces
    Java 8
    Lombok
    CDI 
    Servidor de aplicação Wildfly
    
    
