package br.com.yuri.emissaodeetiquetas.domain;

import java.io.Serializable;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@EqualsAndHashCode
public class Produto implements Serializable {

	private final static long serialVersionUID = 1L;

	private static Integer ultimoCodigo = 0;
	
	@Getter @Setter
	private Integer codigo;
	
	@Getter @Setter
	private String descricao;
	
	@Getter @Setter
	private String preco;

	public Produto(String descricao, String preco) {
		this();
		this.descricao = descricao;
		this.preco = preco;
	}

	public Produto() {
		super();
		this.codigo = Produto.gerarCodigo();
	}

	public static Integer gerarCodigo() {

		return ultimoCodigo ++;
	}

}
