package br.com.yuri.emissaodeetiquetas.factory;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.inject.Produces;

import br.com.yuri.emissaodeetiquetas.domain.Produto;

public class ProdutoFactory {
	
	@Produces 
	public List<Produto> gerarLista() {
		
		return new ArrayList<Produto>();
	}
	
}
