package br.com.yuri.emissaodeetiquetas.managedbean;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.yuri.emissaodeetiquetas.domain.Produto;
import lombok.Getter;
import lombok.Setter;

@Named("produtoBean")
@ViewScoped
public class ProdutoBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject @Getter @Setter
	private Produto produto;
	
	@Getter @Setter
	private Produto produtoSelecionado;

	@Inject @Getter
	private List<Produto> produtos;

	public void adicionar() {

		this.produtos.add(produto);

		this.produto = new Produto();
	}

	public void remover() {

		if (this.produtos.contains(produtoSelecionado))
			this.produtos.remove(produtoSelecionado);
	}

}
